# README A RE-FAIRE #

Mise en place du stack prod

```
#clone repo
git clone git@bitbucket.org:codr-fr/docker-local-env.git docker

#pour changer login et pass mysql
cp .env.dist .env 
vi .env

#fire up
cd docker/lamp-vps
docker-compose up
```

Alias pour utiliser composer

```
alias composer='docker run -ti --rm -v $(pwd):/app composer init'
```

# Legacy #

# ABOUT #

Un premier test de création d'un stack d'environnement de dev local pour remplacer MAMP PRO
Je me suis tres fortement inspiré de nombreux articles du net dont je n'ai plus toutes les sources.
Si je retrouve ca, je rendrais à césar ce qui lui appartient.

## Images ##

apache2
php7-fpm
mysql5.5
phpmyadmin
maildev

## Creation des images ##

Depuis le dossier "images" du repo il faut creer les différentes images

```
docker build apache -t codr/apache
docker build php7-fpm -t codr/php7
docker build mysql -t codr/mysql
docker build phpmyadmin -t codr/phpmyadmin
docker build maildev -t codr/maildev
```

> -t <name> : Le nom de l'image est libre.

## Configuration Apache ##

Dans le dossier <apache/sites-enabled> placer les vhosts.conf

Pensez à rajouter dans votre /etc/hosts local le mapping de vos vhosts

```
127.0.0.1	localhost
127.0.0.1	my.local.dev
```

### Gestion du SSL ###

@TODO

## PHP ##

Dans le dossier <config/etc/php/7.0/fpm/mods-available> 
Modifier xdebug.ini pour y inserer votre IP locale (celle du host pas celle du container) car remote_connect_back ne fonctionne pas bien.
Modifier php-custom.ini pour y apporter vos modification sur php.ini

## MYSQL ##

Exemple de configuration pour symfony : 

```
parameters:
    database_host: codr_mysql
    database_port: null
    database_name: <database>
    database_user: root
    database_password: root
```

## PhpMyAdmin ##

> http://localhost:8036

## MailDev ##

> http://localhost:8025

Aucune configuration n'est requise pour maildev à proprement parlé.
Il faudra simplement configurer votre application pour communiquer avec le container

Exemple pour symfony : 
```
parameters:
    mailer_transport: smtp
    mailer_host: codr_maildev
    mailer_user: null
    mailer_password: null
    secret: ddf6b413cb61633b6
```

## XDebug ##

>J'ai pu faire fonctionner xdebug avec phpStorm.
>Je ne garantie pas le bon fonctionnement avec d'autres IDE

>***Attention: le port utilisé est 9001 au lieu du 9000 habituel
>Le port 9000 est utilisé par php-fpm***

phpStorm : Il faut ajouter un interpretteur php en selectionnant le container php, le reste se fait automatiquement.
Netbeans : http://eldrslump.blogspot.fr/2015/04/enable-debug-with-netbeans-and-apache.html

## Configuration de la composition ##

```
</your/local/www>:/var/www/
```

> Remplacer </your/local/www> avec votre dossier www

Ligne présente 2 fois, une fois dans php et une fois dans apache

## Lancement ##

```
docker-compose up -d
```
> -d pour le lancer en tache de fond

## Execution de commandes php ##

Il faut se connecter au container php

```
docker run -ti <codr_php7>
```

> <codr_php7> correspond au container_name de la composition